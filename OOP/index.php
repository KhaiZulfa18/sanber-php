
<?php 
	require 'Animal.php';
	require 'Frog.php';
	require 'Ape.php';

	$sheep = new Animal("shaun",4,"false");

	echo $sheep->name; // "shaun"
	echo "<br>";
	echo $sheep->legs; // 2
	echo "<br>";
	echo $sheep->cold_blooded; // false
	echo "<br>";
	echo "<br>";

	$frog = new Frog("Katak",4,"true");

	echo $frog->name;
	echo "<br>";
	echo $frog->legs;
	echo "<br>";
	echo $frog->cold_blooded;
	echo "<br>";
	echo $frog->jump();
	echo "<br>";
	echo "<br>";

	$sungokong = new Ape("kera sakti",5,"false");
	echo $sungokong->name;
	echo "<br>";
	echo $sungokong->legs;
	echo "<br>";
	echo $sungokong->cold_blooded;
	echo "<br>";
	echo $sungokong->yell(); // "Auooo"
?>