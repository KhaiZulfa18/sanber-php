<?php 
	/**
	 * 
	 */
	class Animal 
	{
		public $name;
		public $legs;
		public $cold_blooded;
		function __construct($name = "NULL",$legs = 2,$cold_blooded = "false")
		{
			$this->name = $this->getName($name);
			$this->legs = $this->getLegs($legs);
			$this->cold_blooded = $this->getColdBlooded($cold_blooded);
		}

		public function getName($name){
			return $name;
		}

		public function getLegs($legs){
			return $legs;
		}

		public function getColdBlooded($cold_blooded){
			return $cold_blooded;
		}
	}


?>